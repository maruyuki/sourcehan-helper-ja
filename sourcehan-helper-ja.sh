#/bin/sh

# This script will automatically download and install Source Han Japanese language-specific OTFs to your ~/.fonts directory.

cd
if [ ! -d .fonts ]; then
    mkdir .fonts
fi
if [ ! -d SourceHan ]; then
    mkdir SourceHan
fi
cd SourceHan
wget -t 3 -T 60 https://github.com/adobe-fonts/source-han-serif/raw/release/OTF/SourceHanSerifJ_EL-M.zip
unzip ./SourceHanSerifJ_EL-M.zip
mv SourceHanSerifJ_EL-M/SourceHanSerif-*.otf ~/.fonts/
wget -t 3 -T 60 https://github.com/adobe-fonts/source-han-serif/raw/release/OTF/SourceHanSerifJ_SB-H.zip
unzip ./SourceHanSerifJ_SB-H.zip
mv SourceHanSerifJ_SB-H/SourceHanSerif-*.otf ~/.fonts/
wget -t 3 -T 60 https://github.com/adobe-fonts/source-han-sans/raw/release/OTF/SourceHanSansJ.zip
unzip ./SourceHanSansJ.zip
mv SourceHanSans-*.otf ~/.fonts/
fc-cache -v -f .fonts
echo "\nCompleted.\n\nFonts in ~/.fonts:"
ls ~/.fonts
echo "\nDelete ~/SourceHan if you don't need it."
# rm -rf ../SourceHan
